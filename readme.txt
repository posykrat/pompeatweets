=== Manage Privacy Options Page ===
Contributors: CB
Tags: twitter
Requires at least: 5.0
Tested up to: 5.0
Stable tag: 1.0
License: GPLv2
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Pomper des tweets

== Description ==

Rien à déclarer

== Installation ==

1. Upload the entire  folder to the `/wp-content/plugins/` directory.
1. Activate the plugin through the 'Plugins' menu in WordPress.

You will find the setting in the 'Pompe à tweets' settings page

== Frequently Asked Questions ==


== Screenshots ==


== Changelog ==

