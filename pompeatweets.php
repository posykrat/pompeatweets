<?php

/*
Plugin Name: pompeatweets
Description: pomper des tweets
Author: Clément Biron
Version: 0.1
*/

class PompeATweet {

	public function __construct() {
		add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
		add_action( 'admin_init', array( $this, 'page_init' ) );
		add_action( 'acf/save_post', array( $this, 'pomper' ) );
	}

	public function add_plugin_page() {
		add_menu_page(
			'Pompe a tweets', // page_title
			'Pompe a tweets', // menu_title
			'manage_options', // capability
			'pompeatweets_configuration_page', // menu_slug
			array( $this, 'create_configuration_page' ), // function
			'dashicons-admin-generic' // icon_url
		);

		add_submenu_page(
			'pompeatweets_configuration_page',
			'Configuration',
			'Configuration',
			'manage_options',
			'pompeatweets_configuration_page',
			array( $this, 'create_configuration_page' )
		);

		add_submenu_page(
			'pompeatweets_configuration_page',
			'Pomper',
			'Pomper',
			'manage_options',
			'pompeatweets_pomper',
			array( $this, 'create_pomper_page' )
		); 
	}

	public function page_init() {
		acf_form_head();
	}
	
	public function create_configuration_page() {
		do_action('acf/input/admin_head'); // Add ACF admin head hooks
		do_action('acf/input/admin_enqueue_scripts'); // Add ACF scripts
		$options = array(
			'id' => 'acf-form',
			'post_id' => 'options',
			'new_post' => false,
			'field_groups' => array( 'group_5e8ecb31713a6' ),
			'return' => admin_url('admin.php?page=pompeatweets_configuration_page'),
			'submit_value' => 'Enregistrer',
		);
		acf_form( $options );
	}

	public function create_pomper_page(){
		do_action('acf/input/admin_head'); // Add ACF admin head hooks
		do_action('acf/input/admin_enqueue_scripts'); // Add ACF scripts
		$options = array(
			'id' => 'acf-form',
			'post_id' => false,
			'new_post' => false,
			'field_groups' => array( 'group_5e8ed0d2716c6' ),
			'return' => admin_url('admin.php?page=pompeatweets_pomper'),
			'updated_message' => 'Pompage OK.',
			'submit_value' => 'Pomper',
		);
		acf_form( $options );
		
	}

	public function pomper($post_id){

		//Dans l'admin et si on a bien notre repeteur d'url de tweeets complétés
		if(is_admin()){
			$repeater_field_id = 'field_5e8ed0d81ad22';	
			if(isset($_POST['acf'])){
				if(isset($_POST['acf'][$repeater_field_id])){

					//On récupere les settings de twitter API
					$oauth_access_token        = get_field('oauth_access_token','option');
					$oauth_access_token_secret = get_field('oauth_access_token_secret','option');
					$consumer_key              = get_field('consumer_key','option');
					$consumer_secret           = get_field('consumer_secret','option');
					$twitter_oembed_endpoint   = get_field('twitter_oembed_endpoint','option');
					$twitter_settings = array(
						'oauth_access_token'        => $oauth_access_token,
						'oauth_access_token_secret' => $oauth_access_token_secret,
						'consumer_key'              => $consumer_key,
						'consumer_secret'           => $consumer_secret
					);
					require('vendor/twitter-api-php-master/TwitterAPIExchange.php');
					$twitterAPI = new TwitterAPIExchange($twitter_settings);

					if($oauth_access_token && $oauth_access_token_secret && $consumer_key && $consumer_secret && $twitter_oembed_endpoint){
						
						//On parcoure les urls des tweets
						$i = 0;
						$thread = array();	
						$thread['tweets'] = array();
						foreach($_POST['acf'][$repeater_field_id] as $field_data){
							
							//On récuper le tweet
							$tweet_URL = array_values($field_data)[0];
							$tweet = $this->getTweet($tweet_URL, $twitterAPI, $twitter_oembed_endpoint);
							$tweet->html = wp_strip_all_tags($tweet->html);

							//On stocke les infos de base
							if($i === 0){
								$thread['first_tweet_url'] = $tweet_URL;
								$thread['author_name'] = $tweet->author_name;
								$thread['author_url']  = $tweet->author_url;								
							}

							array_push($thread['tweets'], array('field_5e8f035c8a9e9' => $tweet->html, 'field_5e8f03448a9e8' => $tweet_URL));

							$i++;
						}
						
						$this->publishThread($thread);
						
					}	

				}
			}
		}
	}

	private function publishThread($thread){

		//Format title
		$title = $thread['author_name'].' - '.$thread['first_tweet_url'];

		//On configure les post data
		$post_data = array(
			'post_title'  => $title,
			'post_name'   => sanitize_title($title),
			'post_status' => 'publish',
			'post_type'   => 'thread'
		);

		//Si le post n'existe pas déjà
		//if(get_page_by_path(sanitize_title($title), OBJECT, 'thread') === null) {

			//On ajoute le post
			$post_id = wp_insert_post($post_data);
			if( !is_wp_error($post_id) ) {
	
				//On ajoute les terms
				/* wp_set_post_terms($post_id, $ville_term_name, "ville"); */
	
				update_field( 'author_name', $thread['author_name'], $post_id );
				update_field( 'author_url', $thread['author_url'], $post_id );
				update_field( 'field_5e8f032f8a9e7', $thread['tweets'], $post_id );
			}else{
				return $post_id->get_error_message();
			} 
		//}
	}

	private function getTweet($tweet_URL, $twitterAPI, $twitter_oembed_endpoint){
		$getfield      = '?url='.$tweet_URL;
		$requestMethod = 'GET';
		$tweetRequest  = $twitterAPI->setGetfield($getfield)->buildOauth($twitter_oembed_endpoint, $requestMethod)->performRequest();
		$tweet         = json_decode($tweetRequest);
		return $tweet;
	}
}

if (is_admin()){
	$pompe_a_tweet = new PompeATweet();
}

