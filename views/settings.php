<?php
	if(isset($_POST))
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="wrap">

	<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
	
	<form method="post" action="<?php echo esc_html( admin_url( 'admin-post.php' ) ); ?>">

		<div id="universal-message-container">
			<h2>Get tweet</h2>

			<div class="options">
				<p>
					<label>What message would you like to display above each post?</label>
					<br />
					<input type="text" name="acme-message" value="" />
				</p>
		</div><!-- #universal-message-container -->

		<?php
			submit_button();
		?>

	</form>

</div>	